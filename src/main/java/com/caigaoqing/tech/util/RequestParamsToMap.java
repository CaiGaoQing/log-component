package com.caigaoqing.tech.util;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class RequestParamsToMap {
    /**
     * @Author 蔡高情
     * @Description  返回值类型为Map<String, Object>
     * @Date 15:10 2018/8/13 0013
     * @Param [request]
     * @return java.util.Map<java.lang.String,java.lang.Object>
     **/
    public static Map<String, Object> getParameterMap(HttpServletRequest request) {
        Map<String, String[]> properties = request.getParameterMap();//把请求参数封装到Map<String, String[]>中
        Map<String, Object> returnMap = new HashMap<String, Object>();
        Iterator<Entry<String, String[]>> iter = properties.entrySet().iterator();
        String name = "";
        String value = "";
        while (iter.hasNext()) {
            Entry<String, String[]> entry = iter.next();
            name = entry.getKey();
            Object valueObj = entry.getValue();
            if (null == valueObj) {
                value = "";
            } else if (valueObj instanceof String[]) {
                String[] values = (String[]) valueObj;
                //用于请求参数中有多个相同名称
                for (int i = 0; i < values.length; i++) {
                    value = values[i] + ",";
                }
                value = value.substring(0, value.length() - 1);
            } else {
                //用于请求参数中请求参数名唯一
                value = valueObj.toString();
            }
            returnMap.put(name, value);
        }
        return returnMap;
    }
    /**
     * @Author 蔡高情
     * @Description  返回值类型为Map<String, String>
     * @Date 15:11 2018/8/13 0013
     * @Param [request]
     * @return java.util.Map<java.lang.String,java.lang.String>
     **/
    public static Map<String, String> getParameterStringMap(HttpServletRequest request) {
        Map<String, String[]> properties = request.getParameterMap();//把请求参数封装到Map<String, String[]>中
        Map<String, String> returnMap = new HashMap<String, String>();
        String name = "";
        String value = "";
        for (Entry<String, String[]> entry : properties.entrySet()) {
            name = entry.getKey();
            String[] values = entry.getValue();
            if (null == values) {
                value = "";
            } else if (values.length>1) {
                //用于请求参数中有多个相同名称
                for (int i = 0; i < values.length; i++) {
                    value = values[i] + ",";
                }
                value = value.substring(0, value.length() - 1);
            } else {
                //用于请求参数中请求参数名唯一
                value = values[0];
            }
            returnMap.put(name, value);
            
        }
        return returnMap;
    }
    
}