package com.caigaoqing.tech.api;

/**
 * @author 蔡高情
 * @Title: UserInfoApi
 * @ProjectName confusion
 * @Description: 操作用户信息的
 * @date 2018/8/7 000715:32
 */
public interface UserInfoApi {

    public String getUserIdByToken(String token);

}