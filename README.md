# 1.日志组件    

1.需求

​    为了日志规范以及以后的日志分析所要处理的数据，先推想提供公共的面相所有企业的一个日志记录组件，只需要配置可以把数据以规定的样式单独存放，以供以及的日志分析，以及审计操作。目前可能是对写操作比较关注，涉及到日志操作。

2.实现方式

​    面向切面去拦截HTTP请求，里面为考虑到以后对日志进行全量分析，以及目前业务只针对写操作进行审计 ，但是为了考虑通用性（以后考虑进行全量日志分析），所以设计为 所有在切面上并且加上自定义注解的方法才会拦截进行记录日志操作。

4.实现切面，为可虑SpringBoot,和常规WEB项目通用性，项目POM采用最小依赖。（不依赖spring-xxx-start）



3.在很多分布式项目中采用的是token，不是userId,所以定义个接口 去让业务实现。

package com.caigaoqing.tech.api;

/**
 * @author 蔡高情
 * @Title: UserInfoApi
 * @ProjectName confusion
 * @Description: 操作用户信息的
 * @date 2018/8/7 000715:32
 */
public interface UserInfoApi {

    public String getUserIdByToken(String token);

}
